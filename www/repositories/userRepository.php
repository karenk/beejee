<?php

class userRepository extends baseRepository implements interfaceRepository
{
    const TABLE = 'users';
    //user model field
    const USER_TYPE = 'type';
    //fields
    const ID = 'id';
    const NAME = 'name';
    const PASSWORD = 'password';
    const EMAIL = 'email';
    const TYPE_ID = 'typeId';

    public function __construct()
    {
        parent::__construct();
//        parent::getInstance();
    }

    public function find(array $primaryKeys)
    {
        if (!isset($primaryKeys[self::EMAIL])) {
            throw new \InvalidArgumentException(
                'InvalidArgumentException: Expected user email.'
            );
        }
        $query = 'SELECT "' . $this->modelClass . '"'
            . ',    u.' . self::EMAIL . ', u.' . self::NAME . ', u.' . self::PASSWORD
            . ',    u.' . self::EMAIL . ', ut.' . userTypesRepository::NAME . ' AS ' . self::USER_TYPE
            . ' FROM ' . self::TABLE . ' AS u'
            . ' INNER JOIN ' . userTypesRepository::TABLE . ' AS ut'
            . '    WHERE u.' . self::EMAIL . ' = :' . self::EMAIL
            . '    AND u.' . self::TYPE_ID . ' = ut.' . userTypesRepository::ID;

        $stmt = $this->connection->prepare($query);
        $stmt->bindParam(':' . self::EMAIL, $primaryKeys[self::EMAIL]);
        $stmt->execute();


        $stmt->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->modelClass);
        return $stmt->fetch();
    }

    public function findAll()
    {
        $query = 'SELECT "' . $this->modelClass . '"'
            . ',    u.' . self::ID . ', u.' . self::NAME . ', u.' . self::PASSWORD
            . ',    u.' . self::EMAIL . ', ut.' . userTypesRepository::NAME . ' AS ' . self::USER_TYPE
            . ' FROM ' . self::TABLE . ' AS u'
            . ' INNER JOIN ' . userTypesRepository::TABLE . ' AS ut'
            . '    WHERE u.' . self::TYPE_ID . ' = ut.' . userTypesRepository::ID;
        $stmt = $this->connection->prepare($query);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->modelClass);

        return $stmt->fetchAll();
    }

    public function save(\baseModel $comment)
    {
        //Update User if exists
        if (!empty($comment->getEmail())) {
            $findUser = $this->find(array(self::EMAIL => $comment->getEmail()));
            if (is_object($findUser)) {
                $comment->setId($findUser->getId());
                return $this->update($comment);
            }
        }
        //Add new User
        $q1 = 'SELECT ' . userTypesRepository::ID
            . ' FROM ' . userTypesRepository::TABLE
            . ' WHERE ' . userTypesRepository::NAME . '= "' . $comment->getType() . '"';
        $stmt = $this->connection->prepare($q1);
        $stmt->execute();
        $f = $stmt->fetch();
        var_dump($f);
        $typeId = $f[userTypesRepository::ID];

        $query = 'INSERT INTO ' . self::TABLE
            . '(' . self::NAME . ', ' . self::PASSWORD . ', ' . self::EMAIL . ', ' . self::TYPE_ID . ')
            VALUES
            ( :' . self::NAME . ', :' . self::PASSWORD . ', :' . self::EMAIL . ', :' . self::TYPE_ID . ')';
        $stmt = $this->connection->prepare($query);
        $name = $comment->getName();
        $password = $comment->getPassword();
        $email = $comment->getEmail();

        $stmt->bindParam(':' . self::NAME, $name);
        $stmt->bindParam(':' . self::PASSWORD, $password);
        $stmt->bindParam(':' . self::EMAIL, $email);
        $stmt->bindParam(':' . self::TYPE_ID, $typeId);

        return $stmt->execute();
    }

    public function update(\baseModel $user)
    {
        if (!$this->find(array(self::EMAIL => $user->getEmail()))) {
            throw new \InvalidArgumentException(
                'InvalidArgumentException: Cannot update user: ' . $user . ' that does not yet exist in the database . '
            );
        }
        $query = 'UPDATE ' . self::TABLE . ' AS u '
            . 'INNER JOIN(
        SELECT ' . userTypesRepository::ID . ' FROM ' . userTypesRepository::TABLE . '
                WHERE ' . userTypesRepository::TABLE . ' . ' . userTypesRepository::NAME . ' = "' . $user->getType() . '"
             ) AS ut '
            . ' SET u . ' . self::NAME . ' = "' . $user->getName() . '"'
            . ', u . ' . self::PASSWORD . ' = "' . $user->getPassword() . '"'
            . ', u . ' . self::EMAIL . ' = "' . $user->getEmail() . '"'
            . ',  u . ' . self::TYPE_ID . ' = ut . ' . userTypesRepository::ID
            . ' WHERE u . ' . self::ID . ' = "' . $user->getId() . '"';
        $stmt = $this->connection->prepare($query);

        return $stmt->execute();
    }

    public function delete(array $primaryKeys)
    {
//        TODO
    }


    public function getUserByAuth($auth)
    {
        if (!isset($auth[self::EMAIL]) || !isset($auth[self::PASSWORD])) {
            return false;
        }
        $query = 'SELECT * '
            . ' FROM ' . self::TABLE
            . ' WHERE ' . self::EMAIL . '="' . $auth[self::EMAIL] . '"'
            . ' AND ' . self::PASSWORD . '="' . md5($auth[self::PASSWORD]) . '"';
        $stmt = $this->connection->prepare($query);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->modelClass);
        return $stmt->fetch();
    }
}