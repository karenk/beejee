<?php

class commentRepository extends baseRepository implements interfaceRepository
{
    const TABLE = 'comments';
    //user model field
    const COMMENT_STATE = 'state';
    //fields
    const ID = 'id';
    const COMMENT = 'comment';
    const TS = 'ts';
    const USER_NAME = 'userName';
    const USER_EMAIL = 'userEmail';
    const THUMBNAIL = 'thumbnail';
    const STATE_ID = 'stateId';
    private $fetchAllOrderBy;
    private $fetchAllCommentState;

    public function __construct(PDO $connection = null)
    {
        parent::__construct($connection);
        $this->fetchAllOrderBy = 'ts DESC';
        $this->fetchAllCommentState = ' IN ("confirmed","edited") ';
    }

    /**
     * @return string
     */
    public function getFetchAllOrderBy()
    {
        return $this->fetchAllOrderBy;
    }

    /**
     * @param string $fetchAllOrderBy
     */
    public function setFetchAllOrderBy($fetchAllOrderBy)
    {
        $this->fetchAllOrderBy = $fetchAllOrderBy;
    }

    /**
     * @return string
     */
    public function getFetchAllCommentState()
    {
        return $this->fetchAllCommentState;
    }

    /**
     * @param string $fetchAllCommentState
     */
    public function setFetchAllCommentState($fetchAllCommentState)
    {
        $this->fetchAllCommentState = $fetchAllCommentState;
    }

    public function find(array $primaryKeys)
    {
        if (!isset($primaryKeys[self::ID])) {
            throw new \InvalidArgumentException(
                'InvalidArgumentException: Expected user email.'
            );
        }
        $query = 'SELECT "' . $this->modelClass . '"'
            . ',    u.' . self::ID . ', u.' . self::COMMENT . ', u.' . self::TS
            . ',    u.' . self::USER_NAME . ',    u.' . self::USER_EMAIL . ',    u.' . self::THUMBNAIL
            . ', cs.' . commentStateRepository::NAME . ' AS ' . self::COMMENT_STATE
            . ' FROM ' . self::TABLE . ' AS u'
            . ' INNER JOIN ' . commentStateRepository::TABLE . ' AS cs'
            . '    WHERE u.' . self::ID . ' = :' . self::ID
            . '    AND u.' . self::STATE_ID . ' = cs.' . commentStateRepository::ID;

        $stmt = $this->connection->prepare($query);
        $stmt->bindParam(':' . self::ID, $primaryKeys[self::ID]);
        $stmt->execute();

        $stmt->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->modelClass);
        $comment = $stmt->fetch();
        return $comment;
    }

    public function findAll()
    {
        $query = 'SELECT "' . $this->modelClass . '"'
            . ',    u.' . self::ID . ', u.' . self::COMMENT . ', u.' . self::TS
            . ',    u.' . self::THUMBNAIL . ',    u.' . self::USER_EMAIL . ',    u.' . self::USER_NAME
            . ', cs.' . commentStateRepository::NAME . ' AS ' . self::COMMENT_STATE
            . ' FROM ' . self::TABLE . ' AS u'
            . ' INNER JOIN ' . commentStateRepository::TABLE . ' AS cs'
            . ' WHERE u.' . self::STATE_ID . ' = cs.' . commentStateRepository::ID
            . ' AND cs.' . commentStateRepository::NAME . ' ' . $this->fetchAllCommentState
            . '  ORDER BY u.' . $this->fetchAllOrderBy;
        $stmt = $this->connection->prepare($query);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->modelClass);

        return $stmt->fetchAll();
    }

    public function save(\baseModel $comment)
    {
        //get comment state id
        $q1 = 'SELECT ' . commentStateRepository::ID
            . ' FROM ' . commentStateRepository::TABLE
            . ' WHERE ' . commentStateRepository::NAME . '= "' . $comment->getState() . '"';
        $stmt = $this->connection->prepare($q1);
        $stmt->execute();
        $f = $stmt->fetch();
        $stateId = $f[commentStateRepository::ID];
        //Add new Comment
        $query = 'INSERT INTO ' . self::TABLE
            . '(' . self::COMMENT . ', ' . self::USER_NAME . ', ' . self::USER_EMAIL . ', ' . self::THUMBNAIL . ', ' . self::STATE_ID . ', ' . self::TS . ')
            VALUES
            ( :' . self::COMMENT . ', :' . self::USER_NAME . ', :' . self::USER_EMAIL . ', :' . self::THUMBNAIL . ', :' . self::STATE_ID . ', :' . self::TS . ')';
        $stmt = $this->connection->prepare($query);

        $text = $comment->getComment();
        $userName = $comment->getUserName();
        $userEmail = $comment->getUserEmail();
        $thumbnail = $comment->getThumbnail();
        $ts = $comment->getTs();

        $stmt->bindParam(':' . self::COMMENT, $text);
        $stmt->bindParam(':' . self::USER_NAME, $userName);
        $stmt->bindParam(':' . self::USER_EMAIL, $userEmail);
        $stmt->bindParam(':' . self::THUMBNAIL, $thumbnail);
        $stmt->bindParam(':' . self::STATE_ID, $stateId);
        $stmt->bindParam(':' . self::TS, $ts);

        return $stmt->execute();
    }

    public function update(\baseModel $comment)
    {
        if (!$this->find(array(self::ID => $comment->getId()))) {
            throw new \InvalidArgumentException(
                'InvalidArgumentException: Cannot update user: ' . $comment . ' that does not yet exist in the database . '
            );
        }
        $query = 'UPDATE ' . self::TABLE . ' AS u '
            . 'INNER JOIN(
        SELECT ' . commentStateRepository::ID . ' FROM ' . commentStateRepository::TABLE . '
                WHERE ' . commentStateRepository::TABLE . ' . ' . commentStateRepository::NAME . ' = "' . $comment->getState() . '"
             ) AS ut '
            . ' SET u . ' . self::COMMENT . ' = "' . $comment->getComment() . '"'
            . ', u . ' . self::USER_NAME . ' = "' . $comment->getUserName() . '"'
            . ', u . ' . self::USER_EMAIL . ' = "' . $comment->getUserEmail() . '"'
            . ', u . ' . self::THUMBNAIL . ' = "' . $comment->getThumbnail() . '"'
            . ',  u . ' . self::STATE_ID . ' = ut . ' . commentStateRepository::ID
            . ' WHERE u . ' . self::ID . ' = "' . $comment->getId() . '"';
        $stmt = $this->connection->prepare($query);

        return $stmt->execute();
    }

    public function delete(array $primaryKeys)
    {
        // TODO: 
    }

    public function updateJson($json)
    {
        $object = json_decode($json);
        $comment = new commentModel((array)$object);
        $comment->setState('edited');
        $this->update($comment);
    }

    public function saveJson($json)
    {
        $object = json_decode($json);
        $comment = new commentModel((array)$object);
        $this->save($comment);
    }

    public function changeCommentState($args)
    {
        $state = ($args->state) ? 'confirmed' : 'rejected';
        $comment = $this->find(array(self::ID => $args->id));
        $comment->setState($state);
        $this->update($comment);
    }


}