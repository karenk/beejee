<?php
// Loading class
function __autoload($className)
{
    // define a class and find a way for him
    preg_match_all('/((?:^|[A-Z])[a-z]+)/', $className, $matches);
    $expArr = $matches[0];
    $expArr[0] = strtolower($expArr[0]);
    $filename = implode($expArr) . '.php';

    $size = count($expArr);
    $folder = 'application';
    if (!empty($expArr[0] && ($expArr[0] == 'base' || $expArr[0] == 'interface'))) {
    } else if ($size > 1) {
        switch (strtolower($expArr[$size - 1])) {
            case 'controller':
                $folder = 'controllers';
                break;

            case 'model':
                $folder = 'models';
                break;

            case 'repository':
                $folder = 'repositories';
                break;

            default:
                $folder = 'application';
                break;
        }
    }
    // class path
    $file = SITE_PATH . $folder . DS . $filename;
    // check file exists
    if (file_exists($file) == false) {
        return false;
    }
    // include class file
    include($file);
}

try {
    Router::route(new Request);
} catch (Exception $e) {
    $controller = new errorController;
    $controller->error($e->getMessage());
}
