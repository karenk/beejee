<?php

class userModel extends baseModel
{
    private $id;
    private $name;
    private $password;
    private $email;
    private $type;

    public function __construct($data = null)
    {
        if (is_array($data)) {
            if (isset($data['id'])) $this->id = $data['id'];
            $this->name = $data['name'];
            $this->password = $data['password'];
            $this->email = $data['email'];
            $this->type = (isset($data['type'])) ? $data['type'] : 'regular';
        }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed|string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed|string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    function __toString()
    {
        return "{"
        . "\"id\" : \"$this->id\","
        . "\"name\" : \"$this->name\","
        . "\"password\" : \"$this->password\","
        . "\"email\" : \"$this->email\","
        . "\"type\" : \"$this->type\""
        . "}";
    }
}

?>
