<?php

class commentModel extends baseModel
{
    private $id;
    private $comment;
    private $ts;
    private $userName;
    private $userEmail;
    private $thumbnail;
    private $state;

    public function __construct($data = null)
    {
        if (is_array($data)) {
            if (isset($data['id'])) $this->id = $data['id'];
            $this->comment = $data['comment'];
            $this->userName = $data['userName'];
            $this->userEmail = $data['userEmail'];
            $this->ts = time();
            $this->state = 'new';
            if (isset($data['thumbnail'])) $this->thumbnail = $data['thumbnail'];
        }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param mixed $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return int
     */
    public function getTs()
    {
        return $this->ts;
    }

    /**
     * @param int $ts
     */
    public function setTs($ts)
    {
        $this->ts = $ts;
    }

    /**
     * @return mixed
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * @param mixed $userName
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;
    }


    /**
     * @return mixed
     */
    public function getUserEmail()
    {
        return $this->userEmail;
    }

    /**
     * @param mixed $userEmail
     */
    public function setUserEmail($userEmail)
    {
        $this->userEmail = $userEmail;
    }

    /**
     * @return mixed
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * @param mixed $thumbnail
     */
    public function setThumbnail($thumbnail)
    {
        $this->thumbnail = $thumbnail;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    function __toString()
    {
        return "{"
        . "\"id\" : \"$this->id\", "
        . "\"comment\" : \"$this->comment\", "
        . "\"ts\" : \"$this->ts\", "
        . "\"userName\" : \"$this->userName\", "
        . "\"userEmail\" : \"$this->userEmail\", "
        . "\"thumbnail\" : \"$this->thumbnail\", "
        . "\"state\" : \"$this->state\" "
        . "}";
    }
}

?>
