//Controller
var beejeeApp = angular.module('beejeeApp', []);
beejeeApp.controller('beejeeCtrl', function ($scope, $http) {
    $scope.commentsTitle = "Comments";
    $scope.editableComment = {};
    $scope.commentsOrderBy = {
        criterion: 'ts',
        order: 'ASC'
    };
    $scope.selectedOrder = 'ASC';

    //Load All comments
    $scope.getComments = function () {
        $http({
            url: window.location.hostname + "/ajax/getCommentsForAdmin",
            method: "GET",
            params: {
                orderBy: $scope.commentsOrderBy
            },
            headers: {'Accept': 'application/json'}
        })
            .success(function (data, status, headers, config) {
                console.log(data);
                $scope.comments = data;
            })
            .error(function (data, status, headers, config) {
                console.log(status);
                $scope.comments = [];
            });
    }

    $scope.editComment = function (commentId) {
        $http({
            url: window.location.hostname + "/ajax/getComment",
            method: "GET",
            params: {
                id: commentId
            },
            headers: {'Accept': 'application/json'}
        }).success(function (data, status, headers, config) {
            console.log(data);
            $scope.editableComment = data;
        }).error(function (data, status, headers, config) {
            console.log(status);
        });
        angular.element('#edit-modal-btn').click();
        // console.log($scope.editableComment);
    }

    $scope.updateComment = function (comment) {
        console.log(comment);
        $http({
            url: window.location.hostname + "/ajax/updateComment",
            method: "GET",
            params: {
                comment: comment
            },
            headers: {'Accept': 'application/json'}
        }).success(function (data, status, headers, config) {
            console.log(data);
            $scope.editableComment = {};
            $scope.getComments();
        }).error(function (data, status, headers, config) {
            console.log(status);
        });
    }

    $scope.changeState = function (id, state) {
        console.log(state);
        $http({
            url: window.location.hostname + "/ajax/changeCommentState",
            method: "POST",
            params: {
                id: id,
                state: state
            },
            headers: {'Accept': 'application/json'}
        }).success(function (data, status, headers, config) {
            console.log(data);
            $scope.getComments();
        }).error(function (data, status, headers, config) {
            console.log(status);
        });
    }

    $scope.setOrderCriterion = function (order) {
        $scope.commentsOrderBy.criterion = order;
        $scope.getComments();

    }

    $scope.setOrderBy = function () {
        $scope.commentsOrderBy.order = $scope.selectedOrder;
        $scope.getComments();

    }

    $scope.convertCommentTsToDate = function (timestamp) {
        var date = new Date();
        date.setTime(timestamp * 1000);
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var seconds = date.getSeconds();
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();
        return day + "." + month + "." + year + " " + hours + ":" + minutes + ":" + seconds;
    }

    $scope.getComments();
});

$(document).ready(function () {
    $(":file").filestyle();
});