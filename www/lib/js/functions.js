//Controller
var beejeeApp = angular.module('beejeeApp', []);
beejeeApp.directive('ngFiles', ['$parse', function ($parse) {

    function fnLink(scope, element, attrs) {
        var onChange = $parse(attrs.ngFiles);
        element.on('change', function (event) {
            onChange(scope, {$files: event.target.files});
        });
    };

    return {
        link: fnLink
    }
}]);
beejeeApp.controller('beejeeCtrl', function ($scope, $http) {
    $scope.commentsTitle = "Comments";
    $scope.editableComment = {};
    $scope.newComment = {};
    $scope.uploadMessage = '';
    $scope.commentsOrderBy = {
        criterion: 'ts',
        order: 'ASC'
    };
    $scope.selectedOrder = 'ASC';

    var formdata = new FormData();

    $scope.getTheFiles = function ($files) {
        angular.forEach($files, function (value, key) {
            formdata.append(key, value);
        });
    };

    // now upload files
    $scope.uploadFiles = function () {
        // send the files
        $http({
            method: 'POST',
            url: '/upload/image/',
            data: formdata,
            headers: {
                'Content-Type': undefined
            }
        }).success(function (data, status, headers, config) {
            console.log(data);
            $scope.newComment.metas = {};
            $scope.newComment.thumbnail = data.uploaded;
            $scope.uploadMessage = "Image successfuly uploaded.";
            console.log($scope.newComment);
        }).error(function (data, status, headers, config) {
            console.log(status);
        });
    }

    //Load All comments
    $scope.getComments = function () {
        $http({
            url: window.location.hostname + "/ajax/getComments",
            method: "GET",
            params: {
                orderBy: $scope.commentsOrderBy
            },
            headers: {'Accept': 'application/json'}
        })
            .success(function (data, status, headers, config) {
                console.log(data);
                $scope.comments = data;
            })
            .error(function (data, status, headers, config) {
                console.log(status);
                $scope.comments = [];
            });
    }

    $scope.addComment = function (comment) {
        console.log(comment);
        $scope.uploadMessage = '';
        $http({
            url: window.location.hostname + "/ajax/addComment",
            method: "GET",
            params: {
                comment: comment
            },
            headers: {'Accept': 'application/json'}
        }).success(function (data, status, headers, config) {
            console.log(data);
            $scope.newComment = {};
            $scope.getComments();
        }).error(function (data, status, headers, config) {
            console.log(status);
        });
    }

    $scope.convertCommentTsToDate = function (timestamp) {
        var date = new Date();
        date.setTime(timestamp * 1000);
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var seconds = date.getSeconds();
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();
        return day + "." + month + "." + year + " " + hours + ":" + minutes + ":" + seconds;
    }

    $scope.setOrderCriterion = function (order) {
        $scope.commentsOrderBy.criterion = order;
        $scope.getComments();

    }

    $scope.setOrderBy = function () {
        $scope.commentsOrderBy.order = $scope.selectedOrder;
        $scope.getComments();

    }

    $scope.getComments();
});

$(document).ready(function () {
    $(":file").filestyle();
});