<?php

class Router
{
    public static function route(Request $request)
    {
        $controller = $request->getController() . 'Controller';
        $method = $request->getMethod();
        $args = $request->getArgs();

        Router::checkPermission($request->getController());

        $controllerFile = SITE_PATH . 'controllers' . DS . $controller . '.php';
        if (is_readable($controllerFile)) {
            require_once $controllerFile;

            $controller = new $controller;
            $method = (is_callable(array($controller, $method))) ? $method : 'index';

            if (!empty($args)) {
                call_user_func_array(array($controller, $method), array(json_encode($args)));
            } else {
                call_user_func(array($controller, $method));
            }
            return;
        }

        throw new Exception('404 - ' . $request->getController() . ' not found');
    }

    public static function checkPermission($controller)
    {
        if($controller=='admin'){
            $authorization = new authorizationController();
            $authorization->check();
        }
    }
}
