<?php

interface interfaceRepository
{
    public function find(array $primaryKeys);

    public function findAll();

    public function save(\baseModel $object);

    public function update(\baseModel $object);

    public function delete(array $primaryKeys);
}