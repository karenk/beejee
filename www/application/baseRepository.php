<?php

class baseRepository
{
    private static $_instance;
    protected $connection;

    public static function getInstance()
    {
        if (!self::$_instance instanceof self) {
            self::$_instance = new baseRepository;
        }
        return self::$_instance;
    }

    public function __construct(PDO $connection = null)
    {
        $this->connection = $connection;
        if ($this->connection === null) {
            $this->connection = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=' . DB_CHARSET, DB_USER, DB_PASS);
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

            $this->connection->setAttribute(
                PDO::ATTR_ERRMODE,
                PDO::ERRMODE_EXCEPTION
            );
        }
        $this->modelClass = str_replace('Repository', 'Model', static::class);
    }

    public function findAllJson($array)
    {
        $result = '[';
        foreach ($array as $k => $obj) {
            $result .= $obj . ', ';
        }
        $result = substr($result, 0, strlen($result) - 2);
        $result .= ']';
        return $result;
    }
}