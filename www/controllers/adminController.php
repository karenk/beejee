<?php

class adminController extends baseController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $vars['title'] = 'Admin';
        $this->load->view('admin', $vars);
    }

    public function getComment($id)
    {
        $comRepo = new commentRepository();
        echo $comRepo->find(array('id' => 1));
    }
}
