<?php

class uploadController extends baseController
{
    private $folder;
    private $path;
    private $tmpPath;
    // An array of valid file type values
    private $types;
    // Maximum size of file
    private $size;
    private $fileUrl;

    public function __construct()
    {
        parent::__construct();
        $this->folder = DS . 'resources' . DS . 'uploads' . DS . 'images' . DS;
        $this->path = SITE_PATH . 'resources' . DS . 'uploads' . DS . 'images' . DS;
        $this->tmpPath = SITE_PATH . 'resources' . DS . 'uploads' . DS . 'tmp' . DS;
        $this->types = array('image/gif', 'image/png', 'image/jpeg');
        $this->size = 4096000;
    }


    public function index()
    {
        $vars['title'] = 'Rus net soft project';
        $this->load->view('upload', $vars);
    }

    public function image()
    {
        // Processing request
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            //Check the file type
            $file = $_FILES[0];
            if (!in_array($file['type'], $this->types)) {
                die('forbidden file type.');
            }
            // Check the file size
            if ($file['size'] > $this->size) {
                die('Too big file size.');
            }
            $name = $this->resize($file);
            // Download file and display a message
            if (!@copy($this->tmpPath . $name, $this->path . $name)) {
                echo 'Something went wrong';
            } else {
                echo '{"uploaded": "' . $this->fileUrl . '"}';
            }
            // Delete the temporary file
            unlink($this->tmpPath . $name);
        }

    }

    private function generateFileName($file)
    {
        $date = new DateTime();
        $pathArr = explode('.', $file);
        return $pathArr[count($pathArr) - 2] . '_' . $date->getTimestamp() . '.' . $pathArr[count($pathArr) - 1];
    }

    private function resize($file, $quality = null)
    {
        // limit the width in pixels
        $max_thumb_with_size = 320;
        $max_thumb_height_size = 240;

        // default value of quality
        if ($quality == null)
            $quality = 75;

        if ($file['type'] == 'image/jpeg') {
            $source = imagecreatefromjpeg($file['tmp_name']);
        } elseif ($file['type'] == 'image/png') {
            $source = imagecreatefrompng($file['tmp_name']);
        } elseif ($file['type'] == 'image/gif') {
            $source = imagecreatefromgif($file['tmp_name']);
        } else {
            return false;
        }

        $src = $source;
        // determine the width and height of the image
        $w_src = imagesx($src);
        $h_src = imagesy($src);

        $w = $max_thumb_with_size;
        $h = $max_thumb_height_size;
        $fileName = $this->generateFileName($file['name']);
        $this->fileUrl = str_replace('\\', '/', $this->folder . $fileName);
        if ($w_src > $w || $h_src > $h) {
            if ($w / $h > $w_src / $h_src) {
                $ratio = $h_src / $h;
            } else {
                $ratio = $w_src / $w;
            }
            $w_dest = round($w_src / $ratio);
            $h_dest = round($h_src / $ratio);

            // create empty image
            $dest = imagecreatetruecolor($w_dest, $h_dest);

            // copy the old image with the new change of parameters
            imagecopyresampled($dest, $src, 0, 0, 0, 0, $w_dest, $h_dest, $w_src, $h_src);

            // display pictures and purification of memory
            imagejpeg($dest, $this->tmpPath . $fileName, $quality);
            imagedestroy($dest);
            imagedestroy($src);

            return $fileName;
        } else {
            // isplay pictures and purification of memory
            imagejpeg($src, $this->tmpPath . $fileName, $quality);
            imagedestroy($src);

            return $fileName;
        }
    }
}