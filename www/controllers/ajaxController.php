<?php

class ajaxController extends baseController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

    }

    public function getComments($args)
    {
        $args = json_decode($args);
        $args = json_decode($args->orderBy);
        $comRepo = new commentRepository();
        $comRepo->setFetchAllCommentState('IN ("confirmed", "edited")');
        $comRepo->setFetchAllOrderBy($args->criterion . ' ' . $args->order);
        $array = $comRepo->findAll();
        echo $comRepo->findAllJson($array);
    }

    public function getCommentsForAdmin($args)
    {
        $args = json_decode($args);
        $args = json_decode($args->orderBy);
        $comRepo = new commentRepository();
        $comRepo->setFetchAllCommentState('IN ("new", "confirmed", "rejected", "edited")');
        $comRepo->setFetchAllOrderBy($args->criterion . ' ' . $args->order);
        $array = $comRepo->findAll();
        echo $comRepo->findAllJson($array);
    }

    public function getComment($args)
    {
        $args = json_decode($args);
        $comRepo = new commentRepository();
        echo $comRepo->find(array('id' => $args->id));
    }

    public function updateComment($args)
    {
        $args = json_decode($args);
        $comRepo = new commentRepository();
        echo $comRepo->updateJson($args->comment);
    }

    public function addComment($args)
    {
        $args = json_decode($args);
        $comRepo = new commentRepository();
        echo $comRepo->saveJson($args->comment);
    }

    public function changeCommentState($args)
    {
        $args = json_decode($args);
        $comRepo = new commentRepository();
        echo $comRepo->changeCommentState($args);
    }
}
