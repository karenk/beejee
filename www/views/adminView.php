<?php
include 'common/header.php';
?>
    <!--    Custom functions-->
    <script src="/lib/js/admin.js"></script>
    <h2>Admin</h2>
    <div class="row">
        <div class="col-md-12">
            <h2 class="page-header"><h3>{{commentsTitle}}</h3></h2>
            <section class="comment-list">
                <!-- First Comment -->
                <article class="row comments-order">
                    <div class="col-md-12">
                        <h4>Order by</h4>
                        <div class="btn-group" role="group">
                            <button type="button"
                                    ng-class="(commentsOrderBy.criterion == 'userName')? 'ative btn btn-success' : 'btn btn-default'"
                                    ng-click="setOrderCriterion('userName')">user name
                            </button>
                            <button type="button"
                                    ng-class="(commentsOrderBy.criterion == 'userEmail')? 'ative btn btn-success' : 'btn btn-default'"
                                    ng-click="setOrderCriterion('userEmail')">user
                                email
                            </button>
                            <button type="button"
                                    ng-class="(commentsOrderBy.criterion == 'ts')? 'ative btn btn-success' : 'btn btn-default'"
                                    ng-click="setOrderCriterion('ts')">date
                            </button>
                        </div>
                        <div class="btn-group" role="group">
                            <select
                                class="btn btn-info"
                                ng-change="setOrderBy()"
                                ng-model="selectedOrder">
                                <option>ASC</option>
                                <option>DESC</option>
                            </select>
                        </div>
                        <hr>
                    </div>
                </article>
                <article class="row" ng-repeat="comment in comments">
                    <div class="col-md-2 col-sm-2 hidden-xs" ng-if="comment.thumbnail">
                        <figure class="thumbnail">
                            <img class="img-responsive"
                                 src="{{comment.thumbnail}}"/>
                        </figure>
                    </div>
                    <div class="col-md-10 col-sm-10">
                        <div class="panel panel-default arrow left">
                            <div class="panel-body">
                                <header class="text-left">
                                    <div class="comment-user "><strong>{{comment.userName}}</strong></div>
                                    <div class="comment-user ">{{comment.userEmail}}</div>
                                    <time class="comment-date text-muted" datetime="">
                                        {{convertCommentTsToDate(comment.ts)}}
                                    </time>
                                </header>
                                <div class="comment-post">
                                    <hr>
                                    {{ comment.comment }}
                                </div>
                                <div class="text-danger" ng-if="comment.state == 'new'">
                                    <button class="btn btn-danger btn-sm" ng-click="changeState(comment.id, 0)">reject
                                    </button>
                                    |
                                    <button class="btn btn-success btn-sm" ng-click="changeState(comment.id, 1)">
                                        confirm
                                    </button>
                                </div>
                                <div class="text-info text-success" ng-if="comment.state != 'new'">
                                    {{comment.state}}
                                </div>
                                <p class="text-right">
                                    <button class="btn btn-default btn-sm" ng-click="editComment(comment.id)">Edit
                                    </button>
                                </p>
                            </div>
                        </div>
                    </div>
                </article>
            </section>
        </div>
    </div>
    </div>
    <footer>
        <div class="modal-container">
            <!-- Modal -->
            <div id="comment-info-modal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>

                        </div>
                        <div class="modal-body">
                            <form name="editCommentForm" class="form-horizontal" id="comments-form">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="comment-text">Author email: </label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" id="comment-text" name="comment"
                                               ng-model="editableComment.userEmail">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="comment-text">Comment: </label>
                                    <div class="col-sm-10">
                                    <textarea rows="8" class="form-control" id="comment-text" name="comment"
                                              ng-model="editableComment.comment">
                                    </textarea>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-success" data-dismiss="modal"
                                    ng-click="updateComment(editableComment)">Save
                            </button>
                        </div>
                    </div>

                </div>
            </div>
            <button id="edit-modal-btn" type="button" class="btn btn-info btn-lg hidden" data-toggle="modal"
                    data-target="#comment-info-modal">
            </button>
        </div>
    </footer>
<?php
include 'common/footer.php';
?>