<?php
include 'common/header.php';
?>
    <!--    Custom functions-->
    <script src="/lib/js/functions.js"></script>
    <div class="row">
        <div class="col-md-12">
            <h2 class="page-header"><h3>{{commentsTitle}}</h3></h2>
            <section class="comment-list">
                <article class="row comments-order">
                    <div class="col-md-12">
                        <h4>Order by</h4>
                        <div class="btn-group" role="group">
                            <button type="button"
                                    ng-class="(commentsOrderBy.criterion == 'userName')? 'ative btn btn-success' : 'btn btn-default'"
                                    ng-click="setOrderCriterion('userName')">user name
                            </button>
                            <button type="button"
                                    ng-class="(commentsOrderBy.criterion == 'userEmail')? 'ative btn btn-success' : 'btn btn-default'"
                                    ng-click="setOrderCriterion('userEmail')">user
                                email
                            </button>
                            <button type="button"
                                    ng-class="(commentsOrderBy.criterion == 'ts')? 'ative btn btn-success' : 'btn btn-default'"
                                    ng-click="setOrderCriterion('ts')">date
                            </button>
                        </div>
                        <div class="btn-group" role="group">
                            <select
                                class="btn btn-info"
                                ng-change="setOrderBy()"
                                ng-model="selectedOrder">
                                <option>ASC</option>
                                <option>DESC</option>
                            </select>
                        </div>
                        <hr>
                    </div>
                </article>
                <article class="row" ng-repeat="comment in comments">
                    <div class="col-md-2 col-sm-2 hidden-xs" ng-if="comment.thumbnail">
                        <figure class="thumbnail">
                            <img class="img-responsive"
                                 src="{{comment.thumbnail}}"/>
                        </figure>
                    </div>
                    <div class="col-md-10 col-sm-10">
                        <div class="panel panel-default arrow left">
                            <div class="panel-body">
                                <header class="text-left">
                                    <div class="comment-user "><strong>{{comment.userName}}</strong></div>
                                    <div class="comment-user ">{{comment.userEmail}}</div>
                                    <time class="comment-date text-muted" datetime="">
                                        {{convertCommentTsToDate(comment.ts)}}
                                    </time>
                                </header>
                                <div class="comment-post">
                                    <hr>
                                    {{ comment.comment }}
                                </div>
                                <div class="text-info small" ng-if="comment.state == 'edited'">
                                    Changed by admin
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </section>
            <hr>
            <section class="add-coment">
                <form name="addCommentForm" class="form-horizontal" id="comments-form">
                    <div class="form-group">
                        <label ng-if="newComment.thumbnail == undefined" class="col-sm-2 control-label"
                               for="comment-text">
                            Upload image:
                        </label>
                        <div class="col-sm-2 control-label" ng-if="newComment.thumbnail != undefined">
                            <figure class="thumbnail">
                                <img class="img-responsive"
                                     src="{{newComment.thumbnail}}"/>
                            </figure>
                        </div>
                        <div class="col-sm-10 ">
                            <div class="row">
                                <div class="col-md-4">
                                    <input class="filestyle" type="file" id="file1" name="file" multiple
                                           ng-files="getTheFiles($files)" data-icon="false"/>
                                </div>
                                <div class="col-md-1">
                                    <input type="button" ng-click="uploadFiles()" value="Upload"
                                           class="btn btn-default right"/>
                                </div>
                                <div class="col-md-7">{{uploadMessage}}</div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="user-namet">Author name: </label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" id="user-name" name="comment"
                                   ng-model="newComment.userName">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="user-email">Author email: </label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" id="user-email" name="comment"
                                   ng-model="newComment.userEmail">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="comment-text">Comment: </label>
                        <div class="col-sm-10">
                                    <textarea rows="8" class="form-control" id="comment-text" name="comment"
                                              ng-model="newComment.comment">
                                    </textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="comment-text"></label>
                        <div class="col-sm-10">
                            <button type="button" class="btn btn-success" data-dismiss="modal"
                                    ng-click="addComment(newComment)">Add
                            </button>
                        </div>
                    </div>
                </form>
            </section>
        </div>
    </div>
<?php
include 'common/footer.php';
?>