<!DOCTYPE html>
<html lang="en" ng-app="beejeeApp">
<head>
    <meta charset="UTF-8">
    <title></title>
    <!--    Jquery-->
    <script type="text/javascript" src="/lib/js/jquery-3.1.0.min.js"></script>
    <!--    Bootstrap-->
    <link rel="stylesheet" href="/lib/bootstrap-v3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="/lib/bootstrap-v3.3.7/css/bootstrap-theme.min.css">
    <script type="text/javascript" src="/lib/bootstrap-v3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/lib/bootstrap-v3.3.7/js/bootstrap-filestyle.min.js"></script>
    <!--    Styles-->
    <link rel="stylesheet" href="/lib/css/style.css">
    <!--    Angular JS-->
    <script src="/lib/js/angular-1.5.8.min.js"></script>
</head>
<body ng-controller="beejeeCtrl">
<header>
    <div class="container">
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" >BeeJee</a>
                </div>
                <ul class="nav navbar-nav">
                    <li><a href="/index">Home</a></li>
                    <li><a href="/admin">Admin</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="/authorization/logout">logout</a></li>
                    <li><a href="/authorization/login">login</a></li>
                </ul>
            </div>
        </nav>
    </div>
</header>
<div class="container">