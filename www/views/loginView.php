<?php
include 'common/header.php';
?>
    <div class="modal-sm">
        <h2 class="form-signin-heading">Sign In</h2>
        <form class="form-signin" method="post" id="login-form" action="/authorization/login">
            <div class="form-group">
                <input type="text" class="form-control" name="email" placeholder="Your email or login"
                       required/>
                <span id="check-e"></span>
            </div>
            <div class="form-group">
                <input type="password" class="form-control" name="password" placeholder="Your Password"/>
            </div>
            <div class="form-group">
                <button type="submit" name="btn-login" class="btn btn-default">sign in</button>
            </div>
        </form>
    </div>
<?php
include 'common/footer.php';
?>